{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RecordWildCards #-}
-- | Simulate building a module graph
module Simulate
  ( simulate
  , ModEvent (..)
  ) where

import Data.OrdPSQ (OrdPSQ)
import Data.Map.Strict (Map)
import Data.Foldable (foldl')
import Debug.Trace

import WeighedNode

import qualified Data.OrdPSQ as OrdPSQ
import qualified Data.Map.Strict as M
import qualified Data.Set as S


-- | Module info used in our simulation
data SimModInfo =
  SimModInfo
    { simModRevDeps :: [ModName]
    , simNumDeps :: !Int
    , simModTime :: !Double
    }
  deriving (Show)

-- | annotated a module graph with information needed to run the simulation
createSimModInfo :: WeighedGraph -> Map ModName SimModInfo
createSimModInfo wg = foldl' recordRevDeps initialGraph (M.toList wg)
  where
    numDeps = length . filter (`M.member` wg) . nodeDeps
    initialGraph = M.map (\x -> SimModInfo [] (numDeps x) (nodeEopTime x)) wg
    recordRevDeps :: Map ModName SimModInfo -> (ModName, WeighedNode) -> Map ModName SimModInfo
    recordRevDeps !old (modName, WeighedNode {nodeDeps = nodeDeps }) =
      foldl' addRevDep old nodeDeps
      where
        addRevDep !old depName =
          M.adjust (\x -> x { simModRevDeps = modName : simModRevDeps x }) depName old

-- | state of the simulation
data State =
  State
   { stateQueue :: !(OrdPSQ ModName Int SimModInfo)
   , stateActive :: !(OrdPSQ ModName Double (Double, Int, SimModInfo))
   , stateTime :: !Double
   , stateWidth :: !Int -- size of active
   , stateGaps :: !(S.Set Int)
   , stateOut :: ![ModEvent]
   }
 deriving (Show)

data ModEvent = ModEvent
  { eventStart :: !Double
  , eventEnd :: !Double
  , eventModName :: !ModName
  , eventThreadId :: !Int
  }
  deriving (Show)

goSimulate :: State -> Maybe State
goSimulate st@State{..} = case OrdPSQ.minView stateQueue of
  Just (k, 0, minfo@SimModInfo{..}, stateQueue') ->
  -- we can start the build as all dependencies are done
    let
      stateActive' = OrdPSQ.insert k (stateTime + simModTime) (stateTime, threadId, minfo) stateActive
      (stateWidth', stateGaps', threadId) = case S.minView stateGaps of
        Just (threadId, stateGaps') -> (stateWidth, stateGaps', threadId)
        Nothing -> (stateWidth + 1, stateGaps, stateWidth)
    in Just $ st
      { stateQueue = stateQueue'
      , stateActive = stateActive'
      , stateWidth = stateWidth'
      , stateGaps = stateGaps'
      }
  _ -> -- not ready to pop as we are waiting for dependencies
    -- so, we wait for the next module to finish building
    case OrdPSQ.findMin stateActive of
      Nothing -> Nothing
      Just (_, endTime , _) -> Just $ dropDone (st { stateTime = endTime })

-- | drop all finished modules
dropDone
  :: State
  -> State
dropDone st@State{..} =
  case OrdPSQ.minView stateActive of
    Nothing -> st -- empty
    Just (k, end, (startTime, threadId, modSum), active') ->
      if end <= stateTime then
        dropDone
        $ updateRevDeps (simModRevDeps  modSum)
        $ st { stateActive = active'
             , stateOut = ModEvent startTime end k threadId :stateOut
             , stateGaps = S.insert threadId stateGaps
             }
      else
        st

updateRevDeps :: [ModName] -> State -> State
updateRevDeps modNms st = st {stateQueue = foldl' (\o k -> snd $ OrdPSQ.alter f k o) (stateQueue st) modNms}
  where
    f Nothing = ((), Nothing)
    f (Just (p, v)) = ((), Just (p-1,v))

initialState :: WeighedGraph -> State
initialState wg =
  State
    { stateQueue = initialQueue
    , stateActive = OrdPSQ.empty
    , stateTime = 0
    , stateGaps = S.empty
    , stateWidth = 0
    , stateOut = []
    }
  where
    initialQueue =
      OrdPSQ.fromList
      . map (\(k, simModInfo) -> (k, simNumDeps simModInfo, simModInfo))
      . M.toList
      $ createSimModInfo wg

simulate :: WeighedGraph -> [ModEvent]
simulate wg = go (initialState wg)
  where
    go st = case goSimulate st of
      Nothing -> stateOut st
      Just st' -> go st'
