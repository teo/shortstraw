-- |

module ModGraph
  ( ModGraph(..)
  , ModSum(..)
  ) where


data ModGraph =
  ModGraph
    { modSums :: [ModSum]
    }
  deriving (Show)

data ModSum =
  ModSum
    { msModName :: String
    , msTextImports :: [(Maybe String, String)]
    }
  deriving (Show)
