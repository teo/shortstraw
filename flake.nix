{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };
  outputs = {self, nixpkgs}:
    let
      haskellPkgs = nixpkgs.legacyPackages.x86_64-linux.haskellPackages;
      package = haskellPkgs.developPackage {
        root = ./.;
        name = "shortstraw";
      };
    in
      {
      packages.x86_64-linux.shortstraw = package;
      defaultPackage.x86_64-linux = package;
      devShell.x86_64-linux = haskellPkgs.shellFor { 
          packages = p: [ package ];
          nativeBuildInputs = [ haskellPkgs.haskell-language-server haskellPkgs.cabal-install ];
        };
      };
}
