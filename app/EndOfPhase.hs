-- |

module EndOfPhase
  ( EndOfPhase(..)
  ) where

data EndOfPhase = EndOfPhase
  { eopPhaseName :: String
  , eopModuleName :: String
  , eopTime :: Double
  , eopAllocs:: Double
  }
  deriving Show
