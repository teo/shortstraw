{-# LANGUAGE RecordWildCards #-}
module WeighedNode
  ( WeighedNode(..)
  , WeighedGraph
  , ModName
  , mkWeighedGraph
  ) where

import Data.Map.Strict (Map)
import Data.Foldable (foldl')
import ModGraph
import EndOfPhase


import qualified Data.Map as M

type ModName = String

data WeighedNode =
  WeighedNode
    { nodeDeps :: [ModName]
    , nodeEopTime :: !Double
    , nodeAllocs :: !Double
    }
  deriving (Show)

type WeighedGraph = Map ModName WeighedNode

mkWeighedGraph :: ModGraph -> [EndOfPhase] -> WeighedGraph
mkWeighedGraph ModGraph {..} =
  foldl' (flip addWeight) (M.fromList $ map convert modSums)
  where
    convert ModSum {..} =
      (msModName, WeighedNode
        { nodeDeps = map snd msTextImports
        , nodeEopTime = 0
        , nodeAllocs = 0
        })
    addWeight EndOfPhase{..} =
      M.adjust (\wn -> wn {nodeEopTime = eopTime + nodeEopTime wn, nodeAllocs = eopAllocs + nodeAllocs wn}) eopModuleName
