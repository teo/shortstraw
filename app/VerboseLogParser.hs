{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
-- | A parser for GHC's verbose (-v2) log output
module VerboseLogParser
  ( parser
  , toMG
  , toEOP
  ) where

import Control.Monad
import Data.Text.Lazy (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Void
import Data.List ( init, foldl', maximumBy )
import Data.Map (Map)
import Data.Maybe
import Data.Function
import Data.Functor (($>))
import Data.Foldable (traverse_)
import Control.Arrow (first, second)
import Debug.Trace

import WeighedNode
import ModGraph
import EndOfPhase

import qualified Data.Text.Lazy.IO as LazyIO
import qualified Data.Map as M


type Parser = Parsec Void Text

data Line =
  EOP EndOfPhase
  | MG ModGraph
  | Other [Char]
  deriving (Show)

toEOP :: Line -> Maybe EndOfPhase
toEOP (EOP x) = Just x
toEOP _ = Nothing

toMG :: Line -> Maybe ModGraph
toMG (MG x) = Just x
toMG _ = Nothing

isOther :: Line -> Bool
isOther (Other _) = True
isOther _ = False

parser :: Parser [Line]
parser = Prelude.filter (not . isOther) <$>
  many ((try (EOP <$> endOfPhase) <|>
         try (MG <$> modGraph) <|>
         (Other <$> (many printChar))) <* newline) <* eof

endOfPhase :: Parser EndOfPhase
endOfPhase = do
  void $ string "!!! "
  eopPhaseName <- manyTill anySingle (char '[')
  eopModuleName <- manyTill anySingle $ char ']'
  void $ string ": finished in "
  eopTime <- float
  void $ string " milliseconds, allocated "
  eopAllocs <- float
  void $ string " megabytes"
  pure $ EndOfPhase {..}


modSum :: Parser ModSum
modSum = do
  void $ string "NONREC" -- TODO: handle REC
  void space1
  void $ string "ModSummary {"
  void space1
  void $ string "ms_hs_date"
  void $ manyTill anySingle newline -- ignore the rest of the line
  void space1
  void $ string "ms_mod ="
  void space1
  msModName <- many $ satisfy (/= ',') -- ends with ,
  void $ char ','
  void space1
  void $ string "ms_textual_imps ="
  void space1
  msTextImports <- between (char '[') (char ']') $ sepBy import_ (char ',' >> space1)
  void space1
  void $ string "ms_srcimps"
  void $ manyTill anySingle newline -- ignore the rest of the line
  void space1
  void $ char '}'
  pure ModSum {..}

import_ :: Parser (Maybe String, String)
import_ = do
  void $ char '('
  lhs <- (string "Nothing," >> space1 $> Nothing)
     <|> Just <$> (string "Just" *> space1 *> manyTill anySingle (char ',' >> space1)) -- TODO deal with qualified imports
  rhs <- manyTill anySingle (char ')')
  pure (lhs, rhs)

modGraph :: Parser ModGraph
modGraph = do
  void $ string "Ready for upsweep"
  void space1
  modSums <- between (char '[') (char ']') $ modSum `sepBy` (char ',' >> space1)
  pure ModGraph {..}
