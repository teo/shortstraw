{-# LANGUAGE OverloadedStrings #-}
module ChromeTracingExport
  ( mkCompleteEvent
  ) where

import Jsonifier (Json)
import Data.Text (Text)

import qualified Jsonifier as JSON

mkCompleteEvent
  :: Text -- ^ name
  -> Text -- ^ category
  -> Double -- ^ timestamp
  -> Double -- ^ duration
  -> Int -- ^ pid
  -> Int -- ^ thread id
  -> Json
mkCompleteEvent name cat timestamp duration pid tid =
  JSON.object
    [ ("name", JSON.textString name)
    , ("cat" , JSON.textString cat)
    , ("ts" , JSON.doubleNumber timestamp)
    , ("dur" , JSON.doubleNumber duration)
    , ("pid" , JSON.intNumber pid)
    , ("tid" , JSON.intNumber tid)
    , ("ph", JSON.textString "X") -- indicates Complete Event
    ]
